import React from "react";
import './homepage.styles.scss'
import DirectoryComponent from "../../directory/directory.component";


const HomePage = () => (
    <div className="homepage">
       <DirectoryComponent/>
    </div>
)

export default HomePage

